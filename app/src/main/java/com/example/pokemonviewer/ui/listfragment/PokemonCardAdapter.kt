package com.example.pokemonviewer.ui.listfragment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.pokemonviewer.R
import com.example.pokemonviewer.network.domain.Pokemon
import kotlinx.android.synthetic.main.item_card_layout.view.*
import java.util.*

class PokemonCardAdapter(val itemClickListener: OnClickListener): ListAdapter<Pokemon, PokemonCardAdapter.CardViewHolder>(
    PokemonCardDiffCallback()
) {
    class CardViewHolder(val cardView: CardView): RecyclerView.ViewHolder(cardView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        val cardView = LayoutInflater.from(parent.context).inflate(R.layout.item_card_layout, parent, false) as CardView
        return CardViewHolder(cardView)
    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {

        val pokemon = getItem(position)
        holder.cardView.apply {
            pokemonName.text = pokemon.name.capitalize(Locale.getDefault())
            val speciesText = "Species: "  + pokemon.species.name.capitalize(Locale.getDefault())
            pokemonSpecies.text = speciesText

            var hpStat = "HP:"
            var attackStat = "A:"
            var defenseStat = "D:"
            for (item in pokemon.stats) {
                when(item.stat.name) {
                    "hp" -> hpStat += item.baseStat.toString()
                    "attack" -> attackStat += item.baseStat.toString() + " "
                    "defense" -> defenseStat += item.baseStat.toString() + " "
                }
            }
            val statLine = attackStat + defenseStat + hpStat
            pokemonStats.text = statLine

            Glide.with(holder.cardView)
                .load(pokemon.sprites.frontDefault)
                .apply(
                    RequestOptions()
                    .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.ic_pokeball))
                .into(holder.cardView.pokemonImage)
        }

        holder.cardView.setOnClickListener {
            itemClickListener.onClick(it.pokemonName.text.toString().decapitalize(Locale.getDefault()))
        }
    }

    override fun onViewRecycled(holder: CardViewHolder) {
        Glide.with(holder.cardView.context).clear(holder.cardView.pokemonImage)
        super.onViewRecycled(holder)
    }

    class OnClickListener (private val clickListener: (pokemonName: String) -> Unit ) {
        fun onClick(pokemonName: String) = clickListener(pokemonName)
    }
}

class PokemonCardDiffCallback:  DiffUtil.ItemCallback<Pokemon>() {
    override fun areItemsTheSame(oldItem: Pokemon, newItem: Pokemon) = oldItem.name == newItem.name

    override fun areContentsTheSame(oldItem: Pokemon, newItem: Pokemon) = oldItem.name == newItem.name
}