package com.example.pokemonviewer.ui.detailsfragment

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.pokemonviewer.PokemonViewerApplication
import com.example.pokemonviewer.R
import com.example.pokemonviewer.databinding.DetailFragmentBinding
import com.example.pokemonviewer.ui.ViewState

class DetailFragment : Fragment() {

    companion object {
        fun newInstance() = DetailFragment()
    }

    private val viewModel by viewModels<DetailViewModel> { DetailViewModelFactory((requireActivity().application as PokemonViewerApplication).repository) }
    private var _binding: DetailFragmentBinding? = null
    private val binding get() = _binding!!

    val args: DetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DetailFragmentBinding.inflate(inflater, container, false)

        viewModel.viewState.observe(viewLifecycleOwner, ::showViewState)
        viewModel.getPokemonInfo(args.pokemonId)

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()

        Glide.with(this).clear(binding.imageView)
        _binding = null
    }

    private fun showViewState(viewState: ViewState<PokemonInfo>) {
        binding.progressBar.isVisible = viewState is ViewState.Loading

        when (viewState) {
            is ViewState.Content -> {
                val pokemonInfo = viewState.data

                Glide.with(this)
                    .load(pokemonInfo.sprite)
                    .apply(
                        RequestOptions()
                            .placeholder(R.drawable.loading_animation)
                            .error(R.drawable.ic_pokeball))
                    .into(binding.imageView)

                binding.apply {
                    pokemonName.text = pokemonInfo.name
                    pokemonWeight.text = pokemonInfo.weight
                    pokemonHeight.text = pokemonInfo.height
                    pokemonSpecies.text = pokemonInfo.species
                    pokemonType.text = pokemonInfo.types
                    pokemonStats.text = pokemonInfo.stats
                }

            }

            is ViewState.Failed -> Toast.makeText(context, viewState.message, Toast.LENGTH_SHORT).apply {
                    setGravity(Gravity.CENTER, 0, 0)
                    show()
                }
        }
    }
}
