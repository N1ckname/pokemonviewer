package com.example.pokemonviewer.ui.listfragment

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.pokemonviewer.network.domain.Pokemon
import com.example.pokemonviewer.network.domain.PokemonStat
import com.example.pokemonviewer.repository.PokemonRepository
import com.example.pokemonviewer.ui.ResponseContainer
import com.example.pokemonviewer.ui.ViewState
import java.util.*

class ListViewModel(private val repository: PokemonRepository) : ViewModel() {

    private val defaultQuantity = 30

    private var loadedList = mutableListOf<Pokemon>()
    private var resetList = false

    private val _navigateToSelectedItem = MutableLiveData<String>()
    val navigateToSelectedItem: LiveData<String>
        get() = _navigateToSelectedItem

    val viewState: LiveData<ViewState<List<Pokemon>>> =
        Transformations.map(repository.listResponse) { response ->
            when (response) {
                is ResponseContainer.Loading -> ViewState.Loading()
                is ResponseContainer.Success -> {

                    if (resetList) {
                        loadedList = mutableListOf()
                        resetList = false
                    }

                    loadedList.addAll(response.data)
                    ViewState.Content(loadedList.toList())
                }
                is ResponseContainer.Error -> ViewState.Failed(response.error.message.orEmpty())
            }
        }

    init {
        repository.getPokemons(defaultQuantity)
    }

    fun resetList() {
        resetList = true
        repository.resetList(defaultQuantity)
    }

    fun updateList() {
        repository.getPokemons(defaultQuantity)
    }

    fun displayPokemonDetails(pokemonName: String) {
        _navigateToSelectedItem.value = pokemonName
    }

    fun onDisplayPokemonDetailsComplete() {
        _navigateToSelectedItem.value = null
    }

    fun sortList(sortingParams: SortingParams) {
        Log.i("PokemonSort", sortingParams.getEnabledFilters().toString())

        synchronized(loadedList) {

            if (loadedList.isNotEmpty()) {

                var enabledFilters = sortingParams.getEnabledFilters()

                if (enabledFilters.contains(SortFilter.SORT_ASC)) {

                    enabledFilters = enabledFilters.toMutableList()
                    enabledFilters.remove(SortFilter.SORT_ASC)

                    loadedList.sortBy {
                        getPokemonStat(it.stats, enabledFilters)
                    }
                } else {
                    loadedList.sortByDescending {
                        getPokemonStat(it.stats, enabledFilters)
                    }
                }

                viewState.value
            }

            repository.updateList()
       }
    }

    private fun getPokemonStat(pokemonStat: List<PokemonStat>, sortingParams: List<SortFilter>): Int {
        var overallStat = 0
        val sumStat = { condition: Boolean, x: Int -> if (condition) overallStat += x }

        for(item in pokemonStat) {
            when(item.stat.name) {
                "hp" -> sumStat(sortingParams.contains(SortFilter.SORT_BY_HEALTH), item.baseStat)
                "defense" -> sumStat(sortingParams.contains(SortFilter.SORT_BY_DEFENCE), item.baseStat)
                "attack" -> sumStat(sortingParams.contains(SortFilter.SORT_BY_ATTACK), item.baseStat)
            }
        }

        return overallStat / sortingParams.size
    }
}

class SortingParams {
    private val params: EnumMap<SortFilter, Boolean> = EnumMap(SortFilter::class.java)

    init {
        params[SortFilter.SORT_ASC] = true
    }

    fun setParam(filter: SortFilter, value: Boolean = true) {
        params[filter] = value
    }

    fun isValid(): Boolean {
        val enabledFilters = params.values.count { value -> value == true }
        val ascFilter = params[SortFilter.SORT_ASC]!!

        return enabledFilters != 0 && !(enabledFilters == 1 && ascFilter)
    }

    fun getEnabledFilters(): List<SortFilter> = params.keys.filter { params[it]!! }

    fun getSetFilters(): Set<SortFilter> = params.keys

    operator fun get(item: SortFilter): Boolean = params[item] ?: false
}

enum class SortFilter {
    SORT_ASC,
    SORT_BY_ATTACK,
    SORT_BY_DEFENCE,
    SORT_BY_HEALTH
}
