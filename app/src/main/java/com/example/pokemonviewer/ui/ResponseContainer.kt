package com.example.pokemonviewer.ui

sealed class ResponseContainer<v> {

    class Loading<V> : ResponseContainer<V>()

    data class Success<V>(val data: V) : ResponseContainer<V>()

    data class Error<V>(val error: Throwable) : ResponseContainer<V>()
}