package com.example.pokemonviewer.ui.detailsfragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.pokemonviewer.repository.PokemonRepository
import com.example.pokemonviewer.ui.ResponseContainer
import com.example.pokemonviewer.ui.ViewState
import java.util.*

class DetailViewModel(private val repository: PokemonRepository) : ViewModel() {

    val viewState: LiveData<ViewState<PokemonInfo>> =
        Transformations.map(repository.itemResponse) { response ->
            when (response) {
                is ResponseContainer.Loading -> ViewState.Loading()
                is ResponseContainer.Success -> {
                    val pokemon = response.data

                    val types = pokemon.types.joinToString(prefix = "Types: ") { it.type.name }
                    val stats = pokemon.stats.joinToString(
                        prefix = "Stats:\n",
                        separator = "\n"
                    ) { it.stat.name.capitalize(Locale.getDefault()) + " - " + it.baseStat }

                    ViewState.Content(
                        PokemonInfo(
                            pokemon.name.capitalize(Locale.getDefault()),
                            "Weight: ${pokemon.weight} hg",
                            "Height: ${pokemon.height} dm",
                            "Species: ${pokemon.species.name}",
                            types,
                            stats,
                            pokemon.sprites.frontDefault
                        )
                    )
                }
                is ResponseContainer.Error -> ViewState.Failed(response.error.message.orEmpty())
            }
        }

    fun getPokemonInfo(name: String) {
        repository.getPokemon(name)
    }
}

data class PokemonInfo(
    val name: String,
    val weight: String,
    val height: String,
    val species: String,
    val types: String,
    val stats: String,
    val sprite: String?
)
