package com.example.pokemonviewer.ui

sealed class ViewState<V> {

    class Loading<V> : ViewState<V>()

    data class Content<V>(val data: V) : ViewState<V>()

    data class Failed<V>(val message: String) : ViewState<V>()
}