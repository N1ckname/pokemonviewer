package com.example.pokemonviewer.ui.listfragment

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.core.view.doOnLayout
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pokemonviewer.PokemonViewerApplication
import com.example.pokemonviewer.R
import com.example.pokemonviewer.databinding.ListFragmentBinding
import com.example.pokemonviewer.network.domain.Pokemon
import com.example.pokemonviewer.ui.ViewState
import com.google.android.material.chip.Chip
import kotlinx.android.synthetic.main.list_fragment.*

class ListFragment : Fragment() {

    companion object {
        fun newInstance() = ListFragment()
    }

    private val viewModel by viewModels<ListViewModel> { ListViewModelFactory((requireActivity().application as PokemonViewerApplication).repository) }

    private var _binding: ListFragmentBinding? = null
    private val binding get() = _binding!!

    private var optionsMenu: Menu? = null

    private var isLoading: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ListFragmentBinding.inflate(inflater, container, false)

        setHasOptionsMenu(true)

        binding.listFragmentRecyclerView.apply {
            layoutManager = GridLayoutManager(context, 2)
            adapter = PokemonCardAdapter (PokemonCardAdapter.OnClickListener{
                viewModel.displayPokemonDetails(it)
            })

            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)

                    val gridLayoutManager = (recyclerView.layoutManager) as GridLayoutManager
                    val totalItemCount = gridLayoutManager.itemCount
                    val lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition() + 1

                    if (!isLoading && totalItemCount == lastVisibleItem) {
                        optionsMenu?.findItem(R.id.reset_list_button)?.isVisible = false
                        viewModel.updateList()
                    }
                }
            })
        }

        viewModel.viewState.observe(viewLifecycleOwner, ::showViewState)
        viewModel.navigateToSelectedItem.observe(viewLifecycleOwner) {
            if (it != null) {
                findNavController().navigate(ListFragmentDirections.actionListFragmentToDetailFragment(it))
                viewModel.onDisplayPokemonDetailsComplete()
            }
        }

        binding.apply {
            sortingOrderChip.setOnClickListener {
                onChipClickListener()
            }

            attackFilterChip.setOnClickListener {
                onChipClickListener()
            }

            defenceFilterChip.setOnClickListener {
                onChipClickListener()

            }

            healthFilterChip.setOnClickListener {
                onChipClickListener()
            }
        }

        return binding.root
    }

    private fun onChipClickListener() {
        val checkedChips = binding.chipGroup.checkedChipIds

        val sortingParams = SortingParams()
        if (checkedChips.isNotEmpty()) {
            for (id in checkedChips) {
                when (chipGroup.findViewById<Chip>(id)) {
                    binding.attackFilterChip -> sortingParams.setParam(SortFilter.SORT_BY_ATTACK)
                    binding.defenceFilterChip -> sortingParams.setParam(SortFilter.SORT_BY_DEFENCE)
                    binding.healthFilterChip -> sortingParams.setParam(SortFilter.SORT_BY_HEALTH)
                    binding.sortingOrderChip -> sortingParams.setParam(SortFilter.SORT_ASC, false)
                }
            }
        }

        if (sortingParams.isValid()) {
            viewModel.sortList(sortingParams)
        }
    }

    private fun showViewState(viewState: ViewState<List<Pokemon>>) {
        binding.listFragmentProgress.isVisible = viewState is ViewState.Loading
        binding.chipGroup.isVisible = viewState is ViewState.Content

        when (viewState) {
            is ViewState.Loading -> isLoading = true

            is ViewState.Content -> {
                (binding.listFragmentRecyclerView.adapter as PokemonCardAdapter).submitList(viewState.data)
                binding.listFragmentRecyclerView.doOnLayout {
                    onDataLoadComplete()
                    binding.chipGroup.isVisible = true
                }
            }

            is ViewState.Failed -> {
                Toast.makeText(context, viewState.message, Toast.LENGTH_SHORT).apply {
                    setGravity(Gravity.CENTER, 0, 0)
                    show()
                }

                val recyclerViewAdapter = binding.listFragmentRecyclerView.adapter
                if (recyclerViewAdapter != null && recyclerViewAdapter.itemCount > 0) {
                    binding.chipGroup.isVisible = true
                }

                onDataLoadComplete()
            }
        }
    }

    private fun onDataLoadComplete() {
            isLoading = false
            optionsMenu?.findItem(R.id.reset_list_button)?.isVisible = true
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.options_menu, menu)
        optionsMenu = menu

        if (!isLoading) {
            optionsMenu?.findItem(R.id.reset_list_button)?.isVisible = true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        when (item.itemId) {
            R.id.reset_list_button -> {
                item.isVisible = false
                binding.chipGroup.clearCheck()
                viewModel.resetList()

                true
            }
            else -> {
                NavigationUI.onNavDestinationSelected(
                    item,
                    requireView().findNavController()
                ) || super.onOptionsItemSelected(item)
            }
        }

    override fun onDestroyView() {
        super.onDestroyView()
        optionsMenu = null
        _binding = null
    }
}
