package com.example.pokemonviewer

import android.app.Application
import com.example.pokemonviewer.network.PokemonApi
import com.example.pokemonviewer.repository.PokemonRepository

class PokemonViewerApplication: Application() {

    private lateinit var _repository: PokemonRepository
    val repository: PokemonRepository
        get() = _repository

    override fun onCreate() {
        super.onCreate()
        _repository = PokemonRepository.getInstance(PokemonApi.retrofitService)
    }
}