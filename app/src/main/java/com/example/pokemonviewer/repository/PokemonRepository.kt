package com.example.pokemonviewer.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.pokemonviewer.network.PokemonApiService
import com.example.pokemonviewer.network.domain.Item
import com.example.pokemonviewer.network.domain.Pokemon
import com.example.pokemonviewer.network.domain.PokemonList
import com.example.pokemonviewer.ui.ResponseContainer
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.concurrent.thread
import kotlin.random.Random

class PokemonRepository private constructor(private val networkService: PokemonApiService) {

    companion object {
        @Volatile
        private var INSTANCE: PokemonRepository? = null

        fun getInstance(pokemonApiService: PokemonApiService): PokemonRepository {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = PokemonRepository(pokemonApiService)
                    INSTANCE = instance
                }

                return instance
            }
        }
    }

    private val _listResponse = MutableLiveData<ResponseContainer<List<Pokemon>>>(ResponseContainer.Loading())
    val listResponse: LiveData<ResponseContainer<List<Pokemon>>> = _listResponse

    private val _itemResponse = MutableLiveData<ResponseContainer<Pokemon>>(ResponseContainer.Loading())
    val itemResponse: LiveData<ResponseContainer<Pokemon>> = _itemResponse

    private var overallCount = 0
    private var loadedCount = 0
    private var listOffset = 0
    private var resetOffset = false

    fun getPokemons(quantity: Int) {
        _listResponse.value = ResponseContainer.Loading()
        thread {
            _listResponse.postValue(loadPokemons(quantity))
        }
    }

    fun getPokemon(name: String) {
        _itemResponse.value = ResponseContainer.Loading()

        networkService.pokemon(name).enqueue( object : Callback<Pokemon> {
            override fun onResponse(call: Call<Pokemon>, response: Response<Pokemon>) {
                val responseBody = response.body()
                _itemResponse.value = if (responseBody == null) {
                    ResponseContainer.Error(Throwable("Item was not loaded"))
                }
                else {
                     ResponseContainer.Success(responseBody)
                }
            }

            override fun onFailure(call: Call<Pokemon>, t: Throwable) {
                _itemResponse.value = ResponseContainer.Error(t)
            }

        })
    }

    fun resetList(quantity: Int) {
        loadedCount = 0
        overallCount = 0
        resetOffset = true

        getPokemons(quantity)
    }

    private fun loadPokemons(quantity: Int): ResponseContainer<List<Pokemon>> {
        return try {
            if (loadedCount < overallCount || loadedCount == 0 && overallCount == 0) {

                val nextPageSize = if (overallCount != 0 && quantity > (overallCount - loadedCount)) overallCount - loadedCount else quantity

                var response = loadPokemonList(nextPageSize, listOffset)

                if (resetOffset) {
                    val maxOffsetValue = if (quantity < overallCount) (overallCount - quantity + 1) else 1
                    listOffset = Random.nextInt(0, maxOffsetValue)

                    response = loadPokemonList(quantity, listOffset)

                    resetOffset = false
                }

                val responseBody = response.body()
                ResponseContainer.Success(
                    responseBody?.content
                        ?.map(Item::name)
                        ?.mapNotNull(::loadPokemon)
                        .orEmpty().also {
                            loadedCount += it.size
                            listOffset = if (responseBody != null && responseBody.next == null) 0 else (listOffset + it.size)
                        }
                )
            }
            else {
                ResponseContainer.Success(emptyList())
            }
        } catch (error: Throwable) {
            ResponseContainer.Error(error)
        }
    }

    private fun loadPokemonList(quantity: Int, listOffset: Int = 0): Response<PokemonList> =
        networkService.pokemonList(mapOf(Pair("offset", listOffset.toString()), Pair("limit", quantity.toString())))
            .execute()
            .also { overallCount = it.body()?.overallCount ?: 0 }

    private fun loadPokemon(name: String): Pokemon? =
        networkService.pokemon(name).execute().body()

    fun updateList() {
        _listResponse.value = ResponseContainer.Success(emptyList())
    }
}
