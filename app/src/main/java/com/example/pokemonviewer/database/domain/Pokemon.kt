//package com.example.pokemonviewer.database.domain
//
//import androidx.room.Entity
//import androidx.room.PrimaryKey
//
//@Entity(tableName = "pokemon")
//data class Pokemon(
//    @PrimaryKey(autoGenerate = true)
//    val insertId: Long, //used for detection of list offset
//    val id: Int,
//    val name: String,
//    val height: Int,
//    val weight: Int,
//    val species: String,
//    val appearance: PokemonSprites,
//    val stats: List<PokemonStat>,
//    val types: List<PokemonType>
//)
//
//data class PokemonSprites(
//    @PrimaryKey
//    val frontDefault: String,
//    val backDefault: String
//)
//
//data class PokemonStat(
//    @PrimaryKey
//    val name: String,
//    val value: Int
//)
//
//data class PokemonType(
//    @PrimaryKey
//    val name: String,
//    val slot: Int
//)
