//package com.example.pokemonviewer.database
//
//import androidx.room.*
//import com.example.pokemonviewer.network.domain.Pokemon
////import com.example.pokemonviewer.network.domain.PokemonList
//
////@Dao
////interface PokemonListDao {
////    @Query("SELECT * FROM pokemon_list")
////    fun getPokemonList(): PokemonList?
////
////    @Update
////    fun updatePokemonList(list: PokemonList)
////
////    @Insert(onConflict = OnConflictStrategy.REPLACE)
////    fun insertPokemonList(list: PokemonList)
////}
//
//@Dao
//interface PokemonDao {
//    @Query("SELECT * FROM pokemon WHERE id = :id")
//    fun getPokemon(id: Int): Pokemon?
//
//    @Query("SELECT id from pokemon ORDER BY insertId DESC LIMIT 1")
//    fun getLastRecievedId(): Int
//
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    fun insertPokemon(pokemon: Pokemon)
//
//    @Query("DELETE FROM pokemon")
//    fun invalidateCash()
//}
