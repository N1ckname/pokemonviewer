package com.example.pokemonviewer.network.domain

import com.squareup.moshi.Json

data class PokemonList(
    @Json(name = "count")
    val overallCount: Int,
    val next: String?,
    val previous: String?,
    @Json(name = "results")
    val content: List<Item>,
//    @Transient
//    val id: Int = 0,
//    @Transient
//    val shownCount: Int = 0,
//    @Transient
//    val limit: Int = 30,
//    @Transient
//    val offset: Int = 0
)

data class Item(
    val name: String,
    val url: String
)

data class Pokemon(
    val id: Int,
    val name: String,
    val height: Int,
    val weight: Int,
    val species: PokemonSpecies,
    val sprites: PokemonSprites,
    val stats: List<PokemonStat>,
    val types: List<PokemonType>
)

data class PokemonSpecies(
    val name: String
)

data class PokemonSprites(
    @Json(name = "front_default")
    val frontDefault: String?,
    @Json(name = "back_default")
    val backDefault: String?
)

data class PokemonStat(
    val stat: Stat,
    @Json(name = "base_stat")
    val baseStat: Int
)

data class Stat(
    val name: String
)

data class PokemonType(
    val slot: Int,
    val type: Type
)

data class Type(
    val name: String
)
